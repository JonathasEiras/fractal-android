package com.fractal.teste.adapter

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions

@BindingAdapter("img")
fun image(v: ImageView, url: String) {

    Glide.with(v.context)
        .load(url)
        .transition(DrawableTransitionOptions.withCrossFade())
        .into(v)
}