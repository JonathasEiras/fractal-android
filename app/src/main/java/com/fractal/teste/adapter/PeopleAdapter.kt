package com.fractal.teste.adapter

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.fractal.teste.R
import com.fractal.teste.data.Image
import com.fractal.teste.data.Page
import com.fractal.teste.databinding.PeopleItemListBinding
import com.fractal.teste.views.PeopleDetailFragment
import com.fractal.teste.views.PeopleListFragment

class PeopleAdapter(
    private var peopleList: Page,
    internal var context: PeopleListFragment
) : RecyclerView.Adapter<PeopleAdapter.ViewHolder>() {


    val mUrlimages = mutableListOf<Image>()

    fun listUrl() {
        mUrlimages.add(
            Image("https://bit.ly/2srGhRG")
        )
        mUrlimages.add(
            Image("https://bit.ly/2Y19H56")
        )
        mUrlimages.add(
            Image("https://bit.ly/37OBxWD")
        )

        mUrlimages.add(
            Image("https://bit.ly/2DvvKqS")
        )

        mUrlimages.add(
            Image("https://bit.ly/2OUdfli")
        )
        mUrlimages.add(
            Image("https://bit.ly/2qXjaOC")
        )

        mUrlimages.add(
            Image("https://bit.ly/2suDZRP")
        )

        mUrlimages.add(
            Image("https://bit.ly/2q5QkLr")
        )

        mUrlimages.add(
            Image("https://bit.ly/2suEGup")
        )

        mUrlimages.add(
            Image("https://bit.ly/33FrDn3")
        )


    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val v = LayoutInflater.from(parent.context).inflate(R.layout.people_item_list, parent, false)
        return ViewHolder(v)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        listUrl()
        val binding = holder.binding!!
        binding.people = peopleList.results[position]
        binding.image = mUrlimages[position]

        val manager = (holder.itemView.context as FragmentActivity)
        binding.cardItem.setOnClickListener {
            manager.supportFragmentManager.beginTransaction()
                .addSharedElement(binding.imgItem, binding.imgItem.transitionName)
                .replace(
                    R.id.frameContent,
                    PeopleDetailFragment.newInstance(peopleList.results[position].name, mUrlimages[position].url)
                )
                .addToBackStack(null)
                .commit()

            notifyItemChanged(holder.adapterPosition)

        }

        binding.executePendingBindings()
    }


    override fun getItemCount() = peopleList.results.size

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val binding: PeopleItemListBinding? = DataBindingUtil.bind(itemView)

    }


}