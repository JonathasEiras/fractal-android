package com.fractal.teste.api

import com.fractal.teste.data.Page
import com.fractal.teste.data.People
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface PageService {

    @GET("people/?format=json")
    fun page(): Call<Page>

    @GET("people/?format=json")
    fun pages(
        @Query("page") number: String
    ): Call<Page>

    @GET("people/?format=json")
    fun people(
        @Query("search") name: String
    ): Call<People>

}