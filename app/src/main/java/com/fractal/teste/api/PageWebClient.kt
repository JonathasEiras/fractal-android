package com.fractal.teste.api

import com.fractal.teste.data.Page
import com.fractal.teste.data.People
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class PageWebClient {

    fun page(pageResponse: PageResponse) {


        val call = RetrofitInitializer().pageService().page()
        call.enqueue(object : Callback<Page?> {
            override fun onResponse(
                call: Call<Page?>,
                response: Response<Page?>
            ) {
                response.body()?.let {

                    pageResponse.sucess(it)
                }
            }

            override fun onFailure(
                call: Call<Page?>, t: Throwable
            ) {

            }

        })

    }


    fun pageNumber(pageResponse: PageResponse, number: String) {


        val call = RetrofitInitializer().pageService().pages(number)
        call.enqueue(object : Callback<Page?> {
            override fun onResponse(
                call: Call<Page?>,
                response: Response<Page?>
            ) {
                response.body()?.let {
                    pageResponse.sucess(it)

                }
            }

            override fun onFailure(
                call: Call<Page?>, t: Throwable
            ) {

            }

        })


    }


    fun peopleName(peopleResponse: PeopleResponse, name: String) {
        val call = RetrofitInitializer().pageService().people(name)
        call.enqueue(object : Callback<People?> {
            override fun onResponse(
                call: Call<People?>,
                response: Response<People?>
            ) {
                response.body()?.let {
                    peopleResponse.sucessPeopleName(it)
                }
            }

            override fun onFailure(
                call: Call<People?>, t: Throwable
            ) {

            }

        })



    }
}