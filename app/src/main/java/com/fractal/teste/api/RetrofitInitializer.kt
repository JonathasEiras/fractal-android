package com.fractal.teste.api

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitInitializer {


   private fun buildClient(): OkHttpClient{
       return OkHttpClient.Builder()
           .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
           .build()
   }

    private val retrofit = Retrofit.Builder()
        .client(buildClient())
        .baseUrl("https://swapi.co/api/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    fun pageService() = retrofit.create(PageService::class.java)
}