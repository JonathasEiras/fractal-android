package com.fractal.teste.data

data class Image (
    val url: String
)