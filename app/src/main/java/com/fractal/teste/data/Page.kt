package com.fractal.teste.data

data class Page(
    val count: Int,
    val next: String,
    val previous: Any,
    val results: List<People>
)