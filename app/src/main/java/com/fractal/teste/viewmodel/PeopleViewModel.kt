package com.fractal.teste.viewmodel


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fractal.teste.api.PageResponse
import com.fractal.teste.api.PageWebClient
import com.fractal.teste.data.Page
import com.fractal.teste.data.People

class PeopleViewModel : ViewModel() {


    var mPeople: MutableLiveData<People> = MutableLiveData()
    var mPage: MutableLiveData<Page> = MutableLiveData()


    fun getPage(): LiveData<Page> {


        PageWebClient().page(object : PageResponse {
            override fun sucess(page: Page) {
                mPage.value = page
            }
        })
        return mPage
    }

    fun getPeople(name: String): LiveData<People> {
        PageWebClient().page(object : PageResponse {
            override fun sucess(page: Page) {
                for (people: People in page.results) {
                    if (people.name == name) {
                        mPeople.value = people
                    }
                }
            }
        })
        return mPeople
    }

}