package com.fractal.teste.views


import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.app.ActionBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.transition.TransitionInflater
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.fractal.teste.databinding.PeopleItemDetailBinding
import com.fractal.teste.viewmodel.PeopleViewModel

class PeopleDetailFragment : Fragment() {


    private var mName = ""
    private var mUrlImage = ""
    private var toolbar: ActionBar? = null

    companion object {
        fun newInstance(name: String, urlImage: String): PeopleDetailFragment {
            val fragment = PeopleDetailFragment()
            val args = Bundle()
            args.putString("name", name)
            args.putString("url", urlImage)
            fragment.arguments = args
            return fragment
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            sharedElementEnterTransition =
                TransitionInflater.from(context).inflateTransition(android.R.transition.fade)

        }

        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mName = arguments!!.getString("name", "")
            mUrlImage = arguments!!.getString("url", "")
        }



    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {


        val binding = PeopleItemDetailBinding.inflate(inflater, container, false)

        val model = ViewModelProvider(this)[PeopleViewModel::class.java]

        model.getPeople(mName).observe(viewLifecycleOwner, Observer {

            binding.people = it

            image(context!!, mUrlImage, binding.imgDetail)
        })

        binding.toolbarDetails.setNavigationOnClickListener {
            (activity as MainActivity).onBackPressed()
        }
        binding.toolbarDetails.title = mName


        return binding.root
    }


    override fun onStart() {
        super.onStart()
        toolbar = (activity as MainActivity).supportActionBar
        toolbar!!.hide()

    }


    override fun onStop() {
        super.onStop()
        toolbar = (activity as MainActivity).supportActionBar
        toolbar!!.show()
    }


    fun image(context: Context, imageUrl: String, view: ImageView) {
        Glide.with(context)
            .load(imageUrl)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(view)
    }


}



