package com.fractal.teste.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.ActionBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.fractal.teste.adapter.PeopleAdapter
import com.fractal.teste.databinding.RecyclerViewFragmentBinding
import com.fractal.teste.viewmodel.PeopleViewModel

class PeopleListFragment : Fragment() {

    private var toolbar: ActionBar? = null

    companion object {
        fun newInstance(): PeopleListFragment = PeopleListFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding = RecyclerViewFragmentBinding.inflate(inflater, container, false)


        val model = ViewModelProvider(this)[PeopleViewModel::class.java]


        model.getPage().observe(viewLifecycleOwner, Observer {
            val adapter = PeopleAdapter(it, this)
            binding.recyclerView.adapter = adapter
        })
        return binding.root
    }


    override fun onResume() {
        super.onResume()

        toolbar = (activity as MainActivity).supportActionBar
        toolbar?.title = "People"
        toolbar?.setDisplayHomeAsUpEnabled(false)

    }


}